#!/bin/bash
LOKVIDYA_DIRECTORY=/usr/local/bin/lokvidya-desktop-central
IGNORE_FILES_LIST=("test.sh" "test.desktop")
echo running installation ...
array_contains () {
    local array="$1[@]";
    local seeking=$2; shift
    local in=1
    for element in "${!array}"; do
        if [[ $(pwd)/$element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

# delete all existing files in LOKVIDYA_DIRECTORY
echo deleting any existing files
mkdir $LOKVIDYA_DIRECTORY
for filepath in $LOKVIDYA_DIRECTORY/*; do
    echo removing $filepath;
	sudo rm -r $filepath;
done	

# move all files to LOKVIDYA_DIRECTORY
echo copying new files
for filepath in $(pwd)/*; do
	if array_contains IGNORE_FILES_LIST $filepath; then
		echo ignoring $filepath;
	else
		echo copying $filepath;
		sudo cp -R $filepath $LOKVIDYA_DIRECTORY;
	fi
done

# check if jre is installed, if not then install jre
if $(java -version); then
    echo jre already installed
else 
    echo jre not installed
    echo installing jre ...
    sudo apt-get install default-jre
fi

# copy runfile on desktop
sudo cp $(pwd)/runfile.desktop ~/Desktop/
chmod ugo+x ~/Desktop/runfile.desktop

echo installation complete
